# ASAS/ACJC

RUZAFA Rémy
LABBE Nicolas


## Semaine 1 

- Introduction à LORAWAN


## Semaine 2 


### Travail effectué 

- Récupération des travaux effectués les années précédentes
- Observation des interfaces existantes sur `Graphana`, `NodeRed` et sur le `Lora Server`
- Première prise en main de `npm`
- Première prise en main de `docker`
 

## Semaine 3

### Travail effectué

- Prise en main de `NodeJS`
- Suite de la comprehension du travail des années précédentes
 

## Semaine 4

### Travail effectué

- Installation complète de `docker`
- Fonctionnement de Graphana en local
 

## Semaine 5

### Travail effectué

- Modification du NodeRed pour la Serre de Polytech
- Prise en main de `influxDB`


## Semaine 6

### Travail effectué

 - Ajout de la documentation du fonctionnement en local
 - Compréhension de l'utilisation de NodeRed
 - Fonctionnement local de Nodered
 - Génération et analyse de données sur Nodered
 

## Semaine 7

### Travail effectué

 - Fonctionnement total de la chaine en local
 - Compréhension de l'utilisation de influxDB
 - Création de graphs sous Graphana
 - Ajout du capteur de hauteur d'eau sur NodeRed


## Semaine 8

### Travail effectué

- Simulation de données du capteur Tekelek sur NodeRed
- Analyse des données du Tekelek sur NodeRed
- Afichage de données simulées du Tekelek sur Graphana
- Optimisation globale du onctionnement de la base de données : un champ par capteur au lieu d'un champ par donnée
- Modification du NodeRed et de Graphana pour pouvoir utiliser ce nouveau fonctionnement de la base de données

 
## Travail à faire

- Changement du délai d'envoi des messages du capteur Tekelek afin de pouvoir vérifier que notre simulation fonctionne dans la pratique
- Ajouter une possibilité de sauvegarder en cas de fermeture ou de reboot de `docker` pour la Serre de Polytech et de Saint Cassien


 